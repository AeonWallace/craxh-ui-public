import Img from './components/Img';
import Slider from './components/Slider';
import SliderItem from './components/SliderItem';
import Video from './components/Video';
import Dropdown from './components/Dropdown';
import Select from './components/Select';
import VueTouch from 'vue-touch';

const components = [
  Img,
  Slider,
  SliderItem,
  Video,
  Dropdown,
  Select
];

const install = function(Vue, opts = {}) {
  Vue.use(VueTouch);
  components.map(component => {
    Vue.component(component.name, component);
  });
};

export {
  Img,
  Slider,
  SliderItem,
  Video,
  Dropdown,
  Select
}

export default {
  version: '0.0.0',
  install,
  Img,
  Slider,
  SliderItem,
  Video,
  Dropdown,
  Select
};