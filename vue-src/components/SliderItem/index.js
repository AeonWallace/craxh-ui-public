import SliderItem from './SliderItem';

SliderItem.install = function(Vue) {
  Vue.component(SliderItem.name, SliderItem);
}

export default SliderItem;