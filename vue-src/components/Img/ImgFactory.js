import Img from './Img';

export default function ImgFactory(options = {}) {

  console.log('[cxImg]', options);
  return {
    functional: true,
    name: Img.name,
    components: { Img },
    render(createEl, context) {
      return createEl(
        Img, 
        {
          props: {
            ...options,
            ...context.props,
          },
          ...context.data,
        },
        context.children
      )
    },
  }
}