import ImgFactory from './ImgFactory';

/* istanbul ignore next */
export default {
  name: 'cx-img',
  install: function(Vue, options = {}) {
    const Img = ImgFactory(options)
    Vue.component(Img.name, Img);
  }  
};
