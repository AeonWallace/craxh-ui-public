import Video from './Video';

Video.install = function(Vue) {
  Vue.component(Video.name, Video);
}

export default Video;