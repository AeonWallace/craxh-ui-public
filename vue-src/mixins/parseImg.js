import UrlParser from 'url-parse';

export default {
  methods: {
    parseImg: function(imgSrc) {
      const parsedImg = UrlParser(imgSrc);
      parsedImg.query = queryString.parse(parsedImg.query) || {};

      if (this.blackAndWhite) {
        parsedImg.query.impolicy = 'bnw'
      }

      return parsedImg;
    },
  },
}