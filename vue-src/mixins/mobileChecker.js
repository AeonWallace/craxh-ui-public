export default {
  methods: {
    _mc_isStandalone: function() {
      return (navigator && window.navigator.standalone) || !!(typeof window != undefined 
        && window.matchMedia
        && (window.matchMedia('(display-mode: standalone)').matches)
      ) || this._mc_isApp();
    },
    _mc_isIOS: function() {
      return !!(navigator 
        && navigator.userAgent 
        && (navigator.userAgent.match('iPad') ||
          navigator.userAgent.match('iPhone') || 
          navigator.userAgent.match('iPod')
        ));
    },
    _mc_isAndroid: function() {
      return !!(navigator 
        && navigator.userAgent 
        && navigator.userAgent.match('Android'))
    },
    _mc_isApp: function() {
      return !!(navigator 
        && navigator.userAgent 
        && (navigator.userAgent.match('iOSApp') ||
          navigator.userAgent.match('AndroidApp')
        ))
    },
  },
}