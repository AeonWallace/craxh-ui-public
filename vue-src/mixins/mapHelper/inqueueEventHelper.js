export default {
  methods: {      
    $InqueueMapEvent(event) {
      if (!this.$store) return ;

      const _inqueue = (e, p={}) => {
        this.$store.dispatch('inqueueMapEvent', {
          event: e, playload: p
        })
      }

      if (typeof event == 'string') {
        _inqueue(event);
        return ;
      }
      
      if (Array.isArray(event)) {
        event.forEach(e => this.$InqueueMapEvent(e));
        return ;
      }

      if (event && typeof event.event == 'string') {
        const { playload={} } = event;
        _inqueue(event.event, playload);
      }
    },
  },
}