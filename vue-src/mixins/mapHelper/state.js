export default {
  state: [],
  mutations: {
    INQUEUE_MAP_EVENT(state, playload) {
      state.push(playload);
    },
    DEQUEUE_MAP_EVENT(state) {
      state.shift();
    },
  },
  actions: {
    inqueueMapEvent({ commit }, playload) {
      commit('INQUEUE_MAP_EVENT', playload);
    },
    dequeueMapEvent({ commit }) {
      commit('DEQUEUE_MAP_EVENT');
    },
  }
}