import state from './state';
import mapboxgl from 'mapbox-gl';
import { mapState } from 'vuex';

export default {
  data() {
    return {
      projectedPoints: [],
      projectedMarkers: [],
      projectedMarkersID: null,
    }
  },
  computed: {
    ...mapState({
      mapQueue: state => state.mapQueue
    })
  },
  methods: { 
    /**
     * Resize Map
     */
    resizeMap: function() {
      this.map && this.map.resize();
    },

    /**
     * Only consum the event in queue by calling
     * this function
     */
    consumQueue: function() {
      if (this.mapQueue.length === 0) return;

      // Pop next event from queue
      const { event, playload = {} } = this.mapQueue[0];
      console.log(`[MAP HELPER] consum event(s): ${event} with playload:`, playload);
      this.$store.dispatch('dequeueMapEvent');

      // Consum the event
      this[event] && this[event]({...playload});
    },

    /**
     * flatten coordinate in a 1-dem array
     */
    _flattenCoordinates: function(coordinates) {
      return coordinates.reduce((acc, val) => {
        return Array.isArray(val[0]) 
          ? acc.concat(this._flattenCoordinates(val))
          : acc.concat([val])
      }, [])
    },

    /**
     * move the map to a sinlge/group of coordinates
     */
    flyTo: function({center, zoom = 14, padding = 50}) {
      // console.log(`[MAP] Fly To: ${center}, ${zoom}`);
      
      if (!Array.isArray(center[0])) {
        // Single coordinate
        this.map.flyTo({center, zoom});
      } else if (center.length == 1 && center[0].length == 2) {
        // Single coordinate
        this.map.flyTo({center: center[0], zoom});
      } else {
        // Group of coordinates in array
        center = this._flattenCoordinates(center);

        // Construct bounds with given coordinates
        const bounds = new mapboxgl.LngLatBounds();
        center.filter(p => p.length == 2).forEach(p => {
          bounds.extend(p);
        });

        setTimeout(() => {
          this.map.fitBounds([bounds.getSouthWest(), bounds.getNorthEast()], { padding })
        }, 0)
      }
    },

    /**
     * Create CX marker
     */
    createCXMarker: function(info, theme, faIcon) {
      if (!info && !faIcon) {
        faIcon = "fas fa-star"
      }
      const markerDOM = document.createElement('div');
      markerDOM.className = 'cx-icon';
      markerDOM.style.color = theme;
      markerDOM.innerHTML = `<span class="cx-marker__info">${faIcon ? `<i class="${faIcon}"></i>` : info}</span>`;
      return markerDOM;
    },

    /**
     * Project Sights as Points
     */
    projectPoints: function({ points=[], theme="#2ba56b", isOriginal = true }) {
      if (isOriginal) {
        this.projectedPoints.length > 0 && this.removePoints();
      }

      points.forEach(p => {
        if (p.length == 0) return;
        if (Array.isArray(p[0])) {
          this.projectPoints({ p, theme }, false)
        } else {
          const markerDOM = document.createElement('div');
          markerDOM.className = 'map-marker--point'
          if (theme) {
            markerDOM.style = `background-color: ${theme}`
          }
          
          const marker = new mapboxgl.Marker(markerDOM)
            .setLngLat(p)
            .addTo(this.map);
          this.projectedPoints.push(marker);
        }
      });

      console.log(this.projectedPoints);
    },
    removePoints: function() {
      // console.log('[MAP] Remove Projected Sights');
      while (this.projectedPoints.length > 0) {
        const sight = this.projectedPoints.pop();
        sight.remove();
      }
    },

    /**
     * Project Sights as Marker
     */
    projectMarkers: function({ id='unknow', markers=[], theme="#2ba56b", to, bindEvent, startIdx = 0, faIcon=null, force=false}) {
      // console.log(`[MAP] Project Sight as Marker Called, ${markers.length} sights with color ${theme}`);
      if (id == this.projectedMarkersID && id !== 'unknow' && !force) {
        return ;
      } else if (this.projectedMarkersID != null) {
        this.removeMarkers();
      }
      
      const _projectMarker = ({idx, slug, coordinate, sidx='', thumbnail, name, subtitle, description}) => {
        const vm = this;
        const markerDOM = document.createElement('a');
        markerDOM.dataset.id = idx;
        markerDOM.dataset.sidx = sidx;
        markerDOM.dataset.lng = coordinate[0];
        markerDOM.dataset.lat = coordinate[1];
        markerDOM.dataset.theme = theme;
        markerDOM.dataset.active = 0;
        markerDOM.appendChild(faIcon 
          ? this.createCXMarker(0, theme, faIcon)
          : this.createCXMarker(idx, theme));

        let marker = new mapboxgl.Marker(markerDOM)
          .setLngLat(coordinate)
        
        // TODO: use another variable 
        if (thumbnail) {
          let popupHtml = this.createPopup({
            thumbnail,
            title: name,
            subtitle,
            description,
            to: to ? vm.getLocaleUrl(to + slug) : undefined,
          });
  
          let popup = new mapboxgl.Popup({
            closeButton: false,
            // closeOnClick: false,
            className: 'm-popup'
          }).setDOMContent(popupHtml);
          marker.setPopup(popup);
        }

        marker.addTo(this.map)

        // DISABLE marker toggler by click
        // markerDOM.addEventListener('click', function(e) {
        //   e.stopPropagation();
        //   return false;
        // })
        // add reference
        this.projectedMarkers.push(marker);

        // binding event
        if (bindEvent) {
          markerDOM.addEventListener('click', bindEvent(idx, slug, coordinate));
        } else if (to) {
          markerDOM.addEventListener('click', function() {
            vm.$router.push(vm.getLocaleUrl(to + slug));
          });
        } 
      }

      this.$nextTick(() => {
        markers.map((s, idx) => {
          return { ...s, idx: startIdx + idx + 1 }
        }).sort(({coordinate: a}, {coordinate: b}) => {
          if (a[1] == b[1]) return 0;
          return a[1] > b[1] ? -1 : 1;
        }).forEach(({idx, slug, coordinate, thumbnail, name, subtitle, description}) => {
          if (coordinate.length == 0) return;
          _projectMarker({idx, slug, coordinate, thumbnail, name, subtitle, description});
        });
      })
      this.projectedMarkersID = id;
    },
    // TODO: should be sysnc? 
    removeMarkers: function() {
      while (this.projectedMarkers.length > 0) {
        const marker = this.projectedMarkers.pop();
        marker.remove();
      }
      this.projectedMarkersID = null;
    },
    activeMarker: function({ id, dismissOtherActive=true, zoomIn=false, activePopup=false }) {
      // console.log(`[MAP] Active Marker with id: ${id}, dismissOtherActive: ${dismissOtherActive}`);
      dismissOtherActive && this.projectedMarkers.filter(m => {
        return m.getElement().dataset.id != id && m.getElement().dataset.active == 1
      }).forEach(m => {
        this.dismissMarker({id: m.getElement().dataset.id});
      })

      let targets = this.projectedMarkers.filter(m => {
        return m.getElement().dataset.id == id && m.getElement().dataset.active == 0
      })

      zoomIn && targets.length > 0 && this.flyTo({
        center: targets.map(m => {
          let markerDOM = m.getElement();
          return [markerDOM.dataset.lng, markerDOM.dataset.lat]
        })
      })
      
      targets.forEach(m => {
        let markerDOM = m.getElement();
        let popup = m.getPopup();
        let { lng, lat, theme, sidx } = markerDOM.dataset;
        markerDOM.dataset.active = 1;
        markerDOM.firstChild.className = 'cx-marker';
        markerDOM.firstChild.style.color = '';
        markerDOM.firstChild.style.backgroundColor = theme;
        m.remove();
        m = new mapboxgl.Marker(markerDOM, { anchor: 'bottom' })
          .setLngLat([lng, lat])
          .setPopup(popup)
          .addTo(this.map)
        activePopup && m.togglePopup();
      });
    },
    dismissMarker: function({id}) {
      // console.log(`[MAP] Dismiss Marker with id: ${id}`);
      this.projectedMarkers.filter(m => {
        return m.getElement().dataset.id == id && m.getElement().dataset.active == 1
      }).forEach(m => {
        let markerDOM = m.getElement();
        let popup = m.getPopup();
        let { lng, lat, theme } = markerDOM.dataset;
        markerDOM.dataset.active = 0;
        markerDOM.firstChild.className = 'cx-icon';
        markerDOM.firstChild.style.color = theme;
        markerDOM.firstChild.style.backgroundColor = '';
        m.remove();
        m = new mapboxgl.Marker(markerDOM)
          .setLngLat([lng, lat])
          .setPopup(popup)
          .addTo(this.map)
      })
    },
    createPopup: function({ thumbnail, title, subtitle, description, to }) {
      let thumbnailHTML = thumbnail ? `
        <div class="cx-media--cover">
          <div class="cx-media__img"
            style="background-image: url(${thumbnail}"
          ></div>
        </div>
      `: "";

      let titleHTML = title ? `
        <div class="cx-t20">
          ${title}
        </div>
      `: "";

      if (to) {
        titleHTML = `<a class="cx-internal-link" data-internal-href="${to}" href="${to}">${titleHTML}</a>`
      }

      let subtitleHTML = subtitle ? `
        <div class="cx-t10">
          ${subtitle}
        </div>
      `: ""

      let headerHTML = (titleHTML || subtitleHTML) ? `
        <div>
          ${subtitleHTML}
          ${titleHTML}
        </div>
      `: "";

      let descriptionHTML = description ? `
        <div class="-fade cx-t01">
          <p>
            ${description}
          </p>
        </div>
      ` : "";
      
      const articleDOM = document.createElement("div");
      articleDOM.innerHTML = `
        <article>
          <div class="cx-wrap">
            ${headerHTML}
            ${descriptionHTML}
          </div>
          ${thumbnailHTML}
        </article>
      `
      
      let internalLinks = [].slice.call(articleDOM.querySelectorAll('a[data-internal-href]'));
      let vm = this;

      internalLinks.length > 0 && internalLinks.forEach((link) => {
        let routingLink = link.dataset.internalHref;
        link.href = vm.getLocaleUrl(routingLink)
        link.addEventListener('click', function(e) {
          e.preventDefault();
          vm.$router.push(routingLink);
        })  
      })

      articleDOM.className = "popup-wrapper";
      return articleDOM;
    },
  },

  watch: {
    "mapQueue.length": function() {
      this.consumQueue();
    }
  },

  created() {
    this.$store.registerModule('mapQueue', state);
  },
  mounted() {
    this.consumQueue();
  },
}