# CRAXH UI

The easy-to-use starter UI kit made by Craxh.


## Theming CRAXH UI
---

Develope your own theme in this structure. Override default variables in theme-var

```
@import 'theme-var'
@import 'craxh-ui'
@import 'theme-ui'
```


## Typography
---

We use cx-t00, cx-t10... as class name instead of cx-t00, cx-t10 because it is not sutiable to name subtitle on a non subtitle element.
We use it just want to apply the size, so we use a "util"-ish naming method.



|Component|Class|Class
|---|---|---
|Title|cx-t20|cx-t20
|Subitle|cx-t10|cx-t10
|Text|cx-t00|cx-t00
|Sub Text|cx-t01|cx-t01


### Set specific type component style
```
---
```

### Set specific element & it's type component sizes
```
.element{
	@include set-font(.8);
}
```



## Layout
---

grid - 6 columns
breakpoint - xs, sm, md, lg, xl

.cx-col-sm-1, .cx-col-md-4


Style background, type，padding on the outmost container in the theme file

row can use .wrap to put in a container

column is to set the grid

```
<div class="theme-container">
	<div class="cx-row cx-wrap">
		<div class="cx-col"></div>
	</div>
</div>
```

## Breakpoints in variable.scss
---

Use breakpoint variables for media queries. us -max var for max-width queries.

```
// 5. mobile first breakpoint
$breakpoint-xs: 320px !default;
$breakpoint-sm: 576px !default;
$breakpoint-md: 768px !default;
$breakpoint-lg: 1100px !default;
$breakpoint-xl: 2000px !default;

// use in max width
$breakpoint-xs-max: $breakpoint-xs - 1 !default;
$breakpoint-sm-max: $breakpoint-sm - 1 !default;
$breakpoint-md-max: $breakpoint-md - 1 !default;
$breakpoint-lg-max: $breakpoint-lg - 1 !default;
$breakpoint-xl-max: $breakpoint-xl - 1 !default;

// use to build columns
$bpvars:(
    xs: $breakpoint-xs,
    sm: $breakpoint-sm,
    md: $breakpoint-md,
    lg: $breakpoint-lg,
    xl: $breakpoint-xl,
);

$bpvars-max:(
    xs: $breakpoint-xs-max,
    sm: $breakpoint-sm-max,
    md: $breakpoint-md-max,
    lg: $breakpoint-lg-max,
    xl: $breakpoint-xl-max,
);
```